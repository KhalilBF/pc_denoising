//
// Created by khalil on 18-04-05.
//

#include <iostream>
#include <cmath>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/filter.h>
#include "vis_tools.h"

#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/progressive_morphological_filter.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>


#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>

using namespace std ;

void print_help()
{
    pcl::console::print_error("Syntax is: ply_denoising -if inputFilePath -of outputFilePath <options>\n") ;
    pcl::console::print_info ("Options are:\n                     -gr  : with ground removal\n                     -3dv : with 3D visualization\n");
}

int main(int argc, char** argv) {
    string inputFileName, outputFileName, groundRem, visual;
    bool withGroundRemoval, withVisualization;
    withGroundRemoval = withVisualization = false ;

    int ifi, ofi;
    ifi = pcl::console::parse_argument(argc, argv, "-if", inputFileName);
    if (ifi == -1) {
        pcl::console::print_error("missing input .ply file\n");
        print_help();
        return 0;
    } else {
        if (inputFileName.empty()) {
            pcl::console::print_error("missing input .ply file\n");
            print_help();
            return 0;
        } else {
            if (inputFileName[0] == '-') {
                pcl::console::print_error("missing input .ply file\n");
                print_help();
                return 0;
            }
        }
    }
    ofi = pcl::console::parse_argument(argc, argv, "-of", outputFileName);
    if (ofi == -1) {
        pcl::console::print_error("missing output .ply fileName\n");
        print_help();
        return 0;
    } else {
        if (outputFileName.empty()) {
            pcl::console::print_error("missing output .ply file\n");
            print_help();
            return 0;
        } else {
            if (outputFileName[0] == '-') {
                pcl::console::print_error("missing output .ply file\n");
                print_help();
                return 0;
            }
        }
    }
    if (pcl::console::parse_argument(argc, argv, "-gr", groundRem) != -1) {
        withGroundRemoval = true;
    }
    if (pcl::console::parse_argument(argc, argv, "-3dv", visual) != -1) {
        withVisualization = true;
    }
    //cout  << inputFileName << ' ' << outputFileName<< ' ' << withGroundRemoval << ' ' << withVisualization ;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr ground(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr filtered(new pcl::PointCloud<pcl::PointXYZRGB>());

    pcl::PLYReader Reader;
    pcl::console::print_highlight("Loading ");
    pcl::console::print_value("%s ", inputFileName.c_str());
    Reader.read(inputFileName, *cloud);

    cout << *cloud << endl;

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    if (withVisualization) {
        pcl::console::print_highlight("Visualizing original point cloud\n");
        viewer = rgbVis(cloud, "original pcd");
        pcl::console::print_highlight("Close the viewer to continue\n\n");
        while (!viewer->wasStopped()) {
            viewer->spinOnce(100);
            boost::this_thread::sleep(boost::posix_time::microseconds(100000));
        }

    }

    pcl::console::print_highlight("Voxel grid downsamplign\n");
    int K = 0;
    double res = 0;
    res = computeCloudResolution(cloud, K);

    pcl::VoxelGrid<pcl::PointXYZRGB> s;
    pcl::console::print_highlight("Leaf size ");
    pcl::console::print_value("%f \n", 1.5 * res);
    s.setInputCloud(cloud);
    s.setLeafSize(1.5 * res, 1.5 * res, 1.5 * res);
    s.filter(*cloud);

    res = computeCloudResolution(cloud, K);

    cout << *cloud << endl;

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerVG;
    if (withVisualization) {
        pcl::console::print_highlight("Visualizing the down-sampled point cloud\n");
        viewerVG = rgbVis(cloud, "voxel grid downsampling");

        pcl::console::print_highlight("Close the viewer to continue\n\n");
        while (!viewerVG->wasStopped()) {
            viewerVG->spinOnce(100);
            boost::this_thread::sleep(boost::posix_time::microseconds(100000));
        }

    }

    ////////////////////////           First SOR filtering
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    res = computeCloudResolution(cloud, K);
    pcl::console::print_highlight("SOR filtering\n");
    pcl::console::print_highlight("meanK ");
    pcl::console::print_value("%i ", 18 * K);
    pcl::console::print_highlight("Std deviation threshold ");
    pcl::console::print_value("%f\n", 1.5);

    sor.setInputCloud(cloud);
    sor.setMeanK(18 * K);
    sor.setStddevMulThresh(1.5);
    sor.filter(*cloud);
    res = computeCloudResolution(cloud, K);

    cout << *cloud << endl;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerSOR1;
    if (withVisualization) {
        pcl::console::print_highlight("Visualizing SOR-filtered point-cloud\n");
        viewerSOR1 = rgbVis(cloud, "SOR-1");
        pcl::console::print_highlight("Close the viewer to continue\n\n");
        while (!viewerSOR1->wasStopped()) {
            viewerSOR1->spinOnce(100);
            boost::this_thread::sleep(boost::posix_time::microseconds(100000));
        }

    }

    ////////////////////////           Ground removal
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerClusters;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerCluster;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerMls;

    if (withGroundRemoval) {

        pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        pcl::SACSegmentation<pcl::PointXYZRGB> seg;


        seg.setOptimizeCoefficients(true);
        seg.setModelType(pcl::SACMODEL_PLANE);
        seg.setMethodType(pcl::SAC_RANSAC);
        seg.setDistanceThreshold(3 * res);

        seg.setInputCloud(cloud);

        seg.segment(*inliers, *coefficients);

        pcl::console::print_highlight("RANSAC Plan fitting\n");

        pcl::ExtractIndices<pcl::PointXYZRGB> extract;
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(false);

        pcl::console::print_highlight("Extracting the ground\n");
        extract.filter(*ground);
        res = computeCloudResolution(ground, K);

        pcl::console::print_highlight("Extracting ground properties\n");
        pcl::CentroidPoint<pcl::PointXYZRGB> centroid_extractor;
        for (const auto &pg:ground->points) {
            centroid_extractor.add(pg);
        }
        pcl::PointXYZRGB ground_center;
        centroid_extractor.get(ground_center);

        float r, g, b;
        r = g = b = 0;

        for (const auto &pg:ground->points) {
            r += (pg.r - ground_center.r) ^ 2;
            g += (pg.g - ground_center.g) ^ 2;
            b += (pg.b - ground_center.b) ^ 2;
        }
        r /= ground->points.size();
        g /= ground->points.size();
        b /= ground->points.size();


        cout << "average rgb of the ground\n" << (int)ground_center.r << endl << (int)ground_center.g << endl << (int)ground_center.b
             << endl;
        cout << "standard rgb deviations of the ground\n" << sqrt(r) << endl << sqrt(g) << endl << sqrt(b) << endl;

        pcl::search::Search <pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);
        pcl::IndicesPtr indices (new std::vector <int>);
        pcl::PassThrough<pcl::PointXYZRGB> pass;
        pass.setInputCloud (cloud);
        pass.setFilterFieldName ("rgb");
        pass.setFilterLimits (0.0, 1.0);
        pass.filter (*indices);
        pcl::console::print_highlight("Region growing segmentation\n");
        pcl::console::print_highlight("\tdistance threshold: 2*cloud_resolution\n");
        pcl::console::print_highlight("\tmerging rule: color within average rgb of the ground +- its standard deviation\n");
        pcl::console::print_highlight("\tminimum cluster size = 0.5 size of the extracted ground\n");

        pcl::RegionGrowingRGB<pcl::PointXYZRGB> reg;
        reg.setInputCloud (cloud);
        reg.setIndices (indices);
        reg.setSearchMethod (tree);
        reg.setDistanceThreshold (2*res);
        reg.setPointColorThreshold (r+g+b/3);
        reg.setRegionColorThreshold (6);
        reg.setMinClusterSize(int(ground->points.size()/2));

        std::vector <pcl::PointIndices> clusters;
        reg.extract (clusters);

        pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();

        if (withVisualization) {
            pcl::console::print_highlight("Visualizing Clustered point-cloud\n");
            viewerClusters = rgbVis(colored_cloud, "Clusters");
            pcl::console::print_highlight("Close the viewer to continue\n\n");
            while (!viewerClusters->wasStopped()) {
                viewerClusters->spinOnce(100);
                boost::this_thread::sleep(boost::posix_time::microseconds(100000));
            }

        }

        pcl::console::print_highlight("extracting optimal cluster\n");
        extract.setInputCloud (cloud);
        pcl::PointIndices::Ptr best_cluster(new pcl::PointIndices) ;
        pcl::PointIndices::Ptr one_cluster (new pcl::PointIndices);
        pcl::PointXYZRGB c ;
        pcl::CentroidPoint<pcl::PointXYZRGB> cluster_centroid_extractor ;
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cluster_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
        double distance_star = 0 ;
        double distance_to_plan = 0 ;
        int id = 0 ;
        int best_id = 0 ;
        for (auto &cluster : clusters) {
            one_cluster->header = cluster.header ;
            one_cluster->indices = cluster.indices ;
            extract.setIndices (one_cluster);
            extract.filter (*cluster_cloud);


            distance_to_plan = 0 ;
            for(const auto & pg:cluster_cloud->points)
            {
                cluster_centroid_extractor.add(pg) ;
                distance_to_plan += abs(coefficients->values[0]*pg.x + coefficients->values[1]*pg.y+ coefficients->values[2]*pg.z + coefficients->values[3]);
            }

            cluster_centroid_extractor.get(c) ;
            distance_to_plan /= cluster_cloud->points.size() ;


            if(distance_to_plan>distance_star)
            {
                distance_star = distance_to_plan ;
                best_id = id ;  //
            }
            id ++ ;
        }
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr best_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
        //best_cluster->header = clusters[best_id].header ;
        best_cluster->indices = clusters[best_id].indices ;
        extract.setInputCloud (cloud);
        extract.setIndices (best_cluster);
        extract.filter (*best_cloud);

        pcl::copyPointCloud(*best_cloud, *cloud);
        cout << *cloud <<endl ;
        if (withVisualization) {
            pcl::console::print_highlight("Visualizing the optimal cluster\n");
            viewerCluster = rgbVis(cloud, "Optimal cluster");
            pcl::console::print_highlight("Close the viewer to continue\n\n");
            while (!viewerCluster->wasStopped()) {
                viewerCluster->spinOnce(100);
                boost::this_thread::sleep(boost::posix_time::microseconds(100000));
            }

        }
    }



    res = computeCloudResolution(cloud, K) ;

    pcl::console::print_highlight("SOR filtering\n");
    pcl::console::print_highlight("meanK ");
    pcl::console::print_value("%i ", 18 * K);
    pcl::console::print_highlight("Std deviation threshold ");
    pcl::console::print_value("%i\n", 1);

    sor.setInputCloud (cloud);
    sor.setMeanK(18*K);
    sor.setStddevMulThresh (1);
    sor.filter(*cloud);
    res = computeCloudResolution(cloud, K) ;
    cout << *cloud <<endl ;

    pcl::console::print_highlight("SOR filtering\n");
    pcl::console::print_highlight("meanK ");
    pcl::console::print_value("%i ", 18 * K);
    pcl::console::print_highlight("Std deviation threshold ");
    pcl::console::print_value("%i\n", 1);
    sor.setMeanK(18*K);
    sor.setStddevMulThresh (1);
    sor.filter(*cloud);
    res = computeCloudResolution(cloud, K) ;
    cout << *cloud << endl ;

    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree2 (new pcl::search::KdTree<pcl::PointXYZRGB>);

    // Output has the PointNormal type in order to store the normals calculated by MLS
    pcl::PointCloud<pcl::PointXYZRGBNormal> mls_points;
    pcl::console::print_highlight("MLS smoothing ");

    // Init object (second point type is for the normals, even if unused)
    pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> mls;

    mls.setComputeNormals(false);

    // Set parameters
    mls.setInputCloud (cloud);
    //mls.setPolynomialFit (true);
    mls.setPolynomialOrder(2);
    mls.setSearchMethod (tree2);
    mls.setSearchRadius (2*res);
    mls.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGBNormal>::UpsamplingMethod::SAMPLE_LOCAL_PLANE);
    mls.setPointDensity(K+(int)(K/2));
    mls.setUpsamplingRadius(res);
    mls.setUpsamplingStepSize(res/2);


    // Reconstruct
    mls.process (mls_points);

    cout << mls_points ;


    pcl::copyPointCloud(mls_points, *cloud) ;

    if (withVisualization) {
        pcl::console::print_highlight("Visualizing the point-cloud after smoothing and upsampling\n");
        viewerMls = rgbVis(cloud, "MLS");
        pcl::console::print_highlight("Close the viewer to continue\n\n");
        while (!viewerMls->wasStopped())
        {
            viewerMls->spinOnce(100);
            boost::this_thread::sleep(boost::posix_time::microseconds(100000));
        }
    }

    pcl::console::print_highlight("Saving the denoised point cloud\n");
    pcl::io::savePLYFile(outputFileName, *cloud) ;

    return 0;
}