# Compiling the executable
Requirements: PCL library
Starting from ubuntu 18, PCL1.8 is integrated within ubuntu packages. For installation, open a terminal and type:

sudo apt-get install libpcl1 libpcl1-dev

If there are some missing dependencies, type:

sudo apt-get build-dep libpcl1-dev


## In linux:
mkdir build
cd build
cmake ..
make

## In windows:
cd build
cmake ..
nmake (or mingw32-make)

# Usage:

## linux: 
./pcd_denoising -if inputFilePath -of outputFileName -3dv -gr
## windows: 
pcd_denoising -if inputFilePath -of outputFileName -3dv -gr
-3dv (3d visualization) and -gr (ground removal) are optional.

# install PCL 1.8 under linux , debian.
## install java jdk
sudo add-apt-repository -y ppa:webupd8team/java && sudo apt update && sudo apt-get install oracle-java8-installer

## install dependencies
sudo apt-get install g++ cmake cmake-gui doxygen mpi-default-dev openmpi-bin openmpi-common libusb-1.0-0-dev libqhull* libusb-dev libgtest-dev
sudo apt-get install git-core freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libphonon-dev libphonon-dev phonon-backend-gstreamer
sudo apt-get install phonon-backend-vlc graphviz mono-complete qt-sdk libflann-dev

## install flann et boost
sudo apt-get install libflann1.8 libboost1.58-all-dev

## install eigen
cd ~/Downloads
wget http://launchpadlibrarian.net/209530212/libeigen3-dev_3.2.5-4_all.deb
sudo dpkg -i libeigen3-dev_3.2.5-4_all.deb
sudo apt-mark hold libeigen3-dev

## compile and install vtk
wget http://www.vtk.org/files/release/7.1/VTK-7.1.0.tar.gz
tar -xf VTK-7.1.0.tar.gz
cd VTK-7.1.0 && mkdir build && cd build
cmake ..
make
sudo make install

## install pcl
cd ~/Downloads
wget https://github.com/PointCloudLibrary/pcl/archive/pcl-1.8.0.tar.gz
tar -xf pcl-1.8.0.tar.gz
cd pcl-pcl-1.8.0 && mkdir build && cd build
cmake ..
make
sudo make install

## remove useless files
cd ~/Downloads
rm libeigen3-dev_3.2.5-4_all.deb VTK-7.1.0.tar.gz pcl-1.8.0.tar.gz
sudo rm -r VTK-7.1.0 pcl-pcl-1.8.0

